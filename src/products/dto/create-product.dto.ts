import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  id: number;

  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsPositive()
  @IsNotEmpty()
  price: number;
}
